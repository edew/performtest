$nuget = (Resolve-Path .\utilities\nuget.exe)
$nugetParams = 'restore'

$msbuild = 'C:\Program Files (x86)\MSBuild\14.0\Bin\MSBuild.exe'

$iisexpress = "C:\Program Files (x86)\IIS Express\iisexpress.exe"
$site = (Resolve-Path .\WebApp)
$port = 8899
$iisParams = "/port:$port /path:$site"

Start-Process $nuget -ArgumentList $nugetParams -Wait -NoNewWindow
Start-Process $msbuild -Wait -NoNewWindow
Start-Process $iisexpress -ArgumentList $iisParams -Wait -NoNewWindow
