# Edward Dewhurst's Perform Test #

### How do I run this thing? ###

To build the solution and run the site using IISExpress:

* Open a Powershell window
* `Set-Location` to the directory containing this repository e.g. `Set-Location C:\projects\PerformTest`
* Run `build_and_run.ps1`

Note: `build_and_run.ps1` assumes that MSBuild.exe and issexpress.exe are present in the following locations:

`C:\Program Files (x86)\MSBuild\14.0\Bin\MSBuild.exe`
`C:\Program Files (x86)\IIS Express\iisexpress.exe` 

### I ran `build_and_run.ps1` and it built the solution but isn't running IISExpress? ###

Solution: `Ctrl-C` to quit the hanging script, then run `build_and_run.ps1` again.