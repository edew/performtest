﻿using System;
using System.IO;
using System.ServiceModel;
using Autofac;
using Autofac.Integration.Wcf;
using Services;

namespace Loader
{
    public class Program
    {
        static void Main(string[] args)
        {
            var container = GetContainer();

            var app = new LoaderApp(container.Resolve<IImportService>());

            try
            {
                app.Run(args);
            }
            catch (ArgumentException)
            {
                Console.WriteLine("Usage: Loader.exe PATH_TO_DIRECTORY");
                Console.WriteLine("Where PATH_TO_DIRECTORY is a path to the directory containing the node xml files");
            }
            catch (DirectoryNotFoundException)
            {
                Console.WriteLine($"Couldn't find directory {args[0]}");
            }
        }

        private static IContainer GetContainer()
        {
            var builder = new ContainerBuilder();

            builder.Register(c => new ChannelFactory<IImportService>(
                new BasicHttpBinding(),
                new EndpointAddress("http://localhost:8899/services/Import")
                )).SingleInstance();

            builder.Register(c => c.Resolve<ChannelFactory<IImportService>>().CreateChannel())
                .As<IImportService>()
                .UseWcfSafeRelease();

            var container = builder.Build();
            return container;
        }
    }
}
