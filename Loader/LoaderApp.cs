﻿using System;
using System.IO;
using System.Linq;
using Services;

namespace Loader
{
    public class LoaderApp
    {
        private IImportService importService;

        public LoaderApp(IImportService importService)
        {
            this.importService = importService;
        }

        public void Run(string[] args)
        {
            if (args.Length == 0) throw new ArgumentException();

            var nodesXml = Directory.GetFiles(args[0]).Select(File.ReadAllText);
            importService.ImportNodes(nodesXml);
        }
    }
}
