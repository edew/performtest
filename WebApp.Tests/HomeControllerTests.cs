﻿using System.Collections.Generic;
using System.Linq;
using FakeItEasy;
using NUnit.Framework;
using Services;
using TestStack.FluentMVCTesting;
using WebApp.Controllers;

namespace WebApp.Tests
{
    [TestFixture]
    public class HomeControllerTests
    {
        [Test]
        public void IndexActionRespondsWithDefaultView()
        {
            var controller = new HomeController(A.Fake<IJsonService>());

            controller.WithCallTo(c => c.Index()).ShouldRenderDefaultView();
        }

        [Test]
        public void IndexActionCallsGetNodesJsonOnJsonService()
        {
            var jsonService = A.Fake<IJsonService>();

            var controller = new HomeController(jsonService);

            controller.Index();

            A.CallTo(() => jsonService.GetNodesJson()).MustHaveHappened();
        }

        [Test]
        public void IndexActionPassesNodesJsonAsModel()
        {
            var jsonService = A.Fake<IJsonService>();

            A.CallTo(() => jsonService.GetNodesJson()).Returns("I'm some json, trust me!");

            var controller = new HomeController(jsonService);

            controller.WithCallTo(c => c.Index())
                .ShouldRenderDefaultView()
                .WithModel<string>(m => m == "I'm some json, trust me!");
        }
    }
}
