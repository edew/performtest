﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Activation;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Autofac.Integration.Wcf;
using Services;

namespace WebApp
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            var factory = new AutofacWebServiceHostFactory();

            routes.Add(new ServiceRoute("services/Nodes", factory, typeof(NodesService)));
            routes.Add(new ServiceRoute("services/Import", factory, typeof(ImportService)));
            routes.Add(new ServiceRoute("services/Json", factory, typeof(JsonService)));

            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
