﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Services;

namespace WebApp.Controllers
{
    public class HomeController : Controller
    {
        private IJsonService jsonService;

        public HomeController(IJsonService jsonService)
        {
            this.jsonService = jsonService;
        }

        public ActionResult Index()
        {
            var json = jsonService.GetNodesJson();
            return View((object)json);
        }
    }
}