﻿var nodesJson = document.getElementById('model').textContent;

var nodes = JSON.parse(nodesJson);

var nodesForDisplay = nodes.map(function(node) {
    return { id: node.Id, label: node.Label };
});

var edgesForDisplay = nodes.map(function(node) {
    return node.AdjacentNodes.map(function(adjacentNode) {
        return { from: node.Id, to: adjacentNode };
    });
});

// Flatten the edges for display array
edgesForDisplay = Array.prototype.concat.apply([], edgesForDisplay);

var container = document.getElementById('container');
var data = { nodes: nodesForDisplay, edges: edgesForDisplay };

var network = new vis.Network(container, data, {});