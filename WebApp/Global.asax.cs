﻿using System.ServiceModel;
using System.Web.Mvc;
using System.Web.Routing;
using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.Wcf;
using Services;

namespace WebApp
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            var container = GetContainer();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
            AutofacWebServiceHostFactory.Container = container;

            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }

        private static IContainer GetContainer()
        {
            var builder = new ContainerBuilder();
            builder.Register(c => new NodeContext()).AsSelf();

            builder.RegisterType<NodesService>().AsSelf();
            builder.RegisterType<ImportService>().AsSelf();
            builder.RegisterType<JsonService>().AsSelf();

            builder.RegisterControllers(typeof (MvcApplication).Assembly);

            RegisterChannelFactory<INodesService>(builder, "Nodes");
            RegisterChannelFactory<IImportService>(builder, "Import");
            RegisterChannelFactory<IJsonService>(builder, "Json");

            return builder.Build();
        }

        private static void RegisterChannelFactory<T>(ContainerBuilder builder, string endpointSuffix)
        {
            builder.Register(c => new ChannelFactory<T>(
                new BasicHttpBinding(),
                new EndpointAddress($"http://localhost:8899/services/{endpointSuffix}")
            )).SingleInstance();

            builder.Register(c => c.Resolve<ChannelFactory<T>>().CreateChannel())
                .As<T>()
                .UseWcfSafeRelease();
        }
    }
}
