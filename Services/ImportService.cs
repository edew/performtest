﻿using System.Collections.Generic;
using System.Linq;

namespace Services
{
    public class ImportService : IImportService
    {
        private INodesService nodesService;

        public ImportService(INodesService nodesService)
        {
            this.nodesService = nodesService;
        }

        public void ImportNodes(IEnumerable<string> nodesXml)
        {
            var nodes = nodesXml.Select(xml => new Node(xml)).ToArray();

            nodesService.SetNodes(nodes);
        }
    }
}
