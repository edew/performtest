﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Services
{
    public class NodesService : INodesService
    {
        private Func<NodeContext> contextFactory;

        public NodesService(Func<NodeContext> contextFactory)
        {
            this.contextFactory = contextFactory;
        }

        public void SetNodes(IEnumerable<Node> nodes)
        {
            if (nodes == null) throw new ArgumentException();

            using (var context = contextFactory())
            {
                context.Nodes.RemoveRange(context.Nodes);
                context.Nodes.AddRange(nodes);
                context.SaveChanges();
            }
        }

        public Node[] GetNodes()
        {
            using (var context = contextFactory())
            {
                return context.Nodes.ToArray();
            }
        }
    }
}
