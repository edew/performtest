﻿using Newtonsoft.Json;

namespace Services
{
    public class JsonService : IJsonService
    {
        private readonly INodesService nodesService;

        public JsonService(INodesService nodesService)
        {
            this.nodesService = nodesService;
        }

        public string GetNodesJson()
        {
            var nodes = nodesService.GetNodes();

            var json = JsonConvert.SerializeObject(nodes);

            return json;
        }
    }
}
