﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Xml.Linq;
using Newtonsoft.Json;

namespace Services
{
    public class Node
    {
        public Node()
        {
        }

        public Node(string xml)
        {
            if (xml == string.Empty) throw new ArgumentException();

            var xDocument = XDocument.Parse(xml);
            var nodeElement = xDocument.Element("node");

            Id = nodeElement.Element("id").Value;
            Label = nodeElement.Element("label").Value;
            AdjacentNodes = nodeElement.Element("adjacentNodes").Elements("id").Select(e => e.Value).ToArray();
        }

        [Key]
        [JsonIgnore]
        public int DbId { get; set; }
        public string Id { get; set; }
        public string Label { get; set; }

        [NotMapped]
        public string[] AdjacentNodes
        {
            get
            {
                return DbAdjacentNodesString?.Split(';');
            }
            set
            {
                DbAdjacentNodesString = string.Join(";", value.OrderBy(v => v));
            }
        }

        [JsonIgnore]
        public string DbAdjacentNodesString { get; set; }
    }
}