﻿using System.Collections.Generic;
using System.ServiceModel;

namespace Services
{
    [ServiceContract]
    public interface IImportService
    {
        [OperationContract]
        void ImportNodes(IEnumerable<string> nodesXml);
    }
}
