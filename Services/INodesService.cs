﻿using System.Collections.Generic;
using System.ServiceModel;

namespace Services
{
    [ServiceContract]
    public interface INodesService
    {
        [OperationContract]
        void SetNodes(IEnumerable<Node> nodes);

        [OperationContract]
        Node[] GetNodes();
    }
}