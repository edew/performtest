﻿using System.ServiceModel;

namespace Services
{
    [ServiceContract]
    public interface IJsonService
    {
        [OperationContract]
        string GetNodesJson();
    }
}