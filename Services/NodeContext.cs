﻿using System.Data.Entity;

namespace Services
{
    public class NodeContext : DbContext
    {
        public virtual DbSet<Node> Nodes { get; set; }
    }
}
