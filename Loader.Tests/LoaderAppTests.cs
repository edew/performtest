﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using FakeItEasy;
using NUnit.Framework;
using Services;

namespace Loader.Tests
{
    [TestFixture]
    public class LoaderAppTests
    {
        [Test]
        public void RunThrowsArgumentExceptionForEmptyArgs()
        {
            var app = new LoaderApp(A.Fake<IImportService>());

            Assert.Throws<ArgumentException>(() => app.Run(new string[0]));
        }

        [Test]
        public void RunPassesXmlFromDirectoryPathInFirstArgToImportService()
        {
            var importService = A.Fake<IImportService>();
            var directoryPath = "TestData";
            var app = new LoaderApp(importService);

            app.Run(new [] { directoryPath });

            var expected = Directory.GetFiles(directoryPath).Select(File.ReadAllText);

            A.CallTo(() => importService.ImportNodes(A<IEnumerable<string>>.That.IsSameSequenceAs(expected)))
                .MustHaveHappened();
        }
    }
}
