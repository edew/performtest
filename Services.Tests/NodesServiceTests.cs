﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using FakeItEasy;
using NUnit.Framework;

namespace Services.Tests
{
    [TestFixture]
    public class NodesServiceTests
    {
        [Test]
        public void SetNodesThrowsArgumentExceptionForNullInput()
        {
            var nodesService = new NodesService(() => A.Fake<NodeContext>());

            Assert.Throws<ArgumentException>(() => nodesService.SetNodes(null));
        }

        [Test]
        public void SetNodesAddsInputNodesToContextCollection()
        {
            var context = FakeNodeContext(new Node[] {});

            var nodesService = new NodesService(() => context);
     
            var input = new[] {new Node { Id = "1" } };
            nodesService.SetNodes(input);

            A.CallTo(() => context.Nodes.AddRange(A<IEnumerable<Node>>.That.Matches(n => n.Single().Id == "1")))
                .MustHaveHappened();
        }

        [Test]
        public void SetNodesClearsExistingContextCollection()
        {
            var nodes = new[] {new Node {Id = "111"}};
            var context = FakeNodeContext(nodes);

            var nodesService = new NodesService(() => context);
            nodesService.SetNodes(new Node[] {});

            A.CallTo(() => context.Nodes.RemoveRange(A<IEnumerable<Node>>.That.Matches(n => n.Single().Id == "111")))
                .MustHaveHappened();
        }

        [Test]
        public void SetNodesClearsExistingContextBeforeAddingNewNodes()
        {
            var nodes = new[] { new Node { Id = "111" } };
            var context = FakeNodeContext(nodes);

            var input = new[] { new Node { Id = "222"} };
            var nodesService = new NodesService(() => context);
            nodesService.SetNodes(input);

            A.CallTo(() => context.Nodes.RemoveRange(A<IEnumerable<Node>>.That.Matches(n => n.Single().Id == "111")))
                .MustHaveHappened();

            A.CallTo(() => context.Nodes.AddRange(A<IEnumerable<Node>>.That.Matches(n => n.Single().Id == "222")))
                .MustHaveHappened();
        }

        [Test]
        public void SetNodesSavesChangesAfterUpdating()
        {
            var nodes = new[] { new Node { Id = "8888" } };
            var context = FakeNodeContext(nodes);

            var input = new[] { new Node { Id = "9999" } };
            var nodesService = new NodesService(() => context);
            nodesService.SetNodes(input);

            A.CallTo(() => context.Nodes.RemoveRange(A<IEnumerable<Node>>.That.Matches(n => n.Single().Id == "8888")))
                .MustHaveHappened();

            A.CallTo(() => context.Nodes.AddRange(A<IEnumerable<Node>>.That.Matches(n => n.Single().Id == "9999")))
                .MustHaveHappened();

            A.CallTo(() => context.SaveChanges()).MustHaveHappened();
        }

        [Test]
        public void GetNodesReturnsNodesCollectionFromContext()
        {
            var nodes = new[] {new Node {Id = "11"}, new Node {Id = "22"}, new Node { Id = "33" } };
            var context = FakeNodeContext(nodes);

            var nodesService = new NodesService(() => context);

            var actual = nodesService.GetNodes();

            CollectionAssert.AreEquivalent(nodes, actual);
        }

        private static NodeContext FakeNodeContext(IEnumerable<Node> nodes)
        {
            var nodesSet = FakeNodesSet(nodes);
            var context = A.Fake<NodeContext>();

            A.CallTo(() => context.Nodes).Returns(nodesSet);
            return context;
        }

        private static DbSet<Node> FakeNodesSet(IEnumerable<Node> nodes)
        {
            var queryable = nodes.AsQueryable();
            var nodesSet = A.Fake<DbSet<Node>>(fake => fake.Implements<IQueryable<Node>>());
            A.CallTo(() => ((IQueryable<Node>) nodesSet).Provider).Returns(queryable.Provider);
            A.CallTo(() => ((IQueryable<Node>) nodesSet).Expression).Returns(queryable.Expression);
            A.CallTo(() => ((IQueryable<Node>) nodesSet).ElementType).Returns(queryable.ElementType);
            A.CallTo(() => ((IQueryable<Node>) nodesSet).GetEnumerator()).Returns(queryable.GetEnumerator());
            return nodesSet;
        }
    }
}
