﻿using System;
using NUnit.Framework;
using System.IO;

namespace Services.Tests
{
    [TestFixture]
    public class NodeTests
    {
        private Node amazonNode;
        private Node appleNode;

        [SetUp]
        public void SetUp()
        {
            this.amazonNode = new Node(File.ReadAllText("TestData\\amazon.xml"));
            this.appleNode = new Node(File.ReadAllText("TestData\\apple.xml"));
        }

        [Test]
        public void CanBeConstructedUsingParameterlessConstructor()
        {
            var node = new Node();

            Assert.IsInstanceOf<Node>(node);
        }

        [Test]
        public void ThrowsArgumentExceptionWithEmptyStringConstructorInput()
        {
            Assert.Throws<ArgumentException>(() => new Node(string.Empty));
        }

        [Test]
        public void SetsAmazonNodeIdTo9()
        {
            Assert.AreEqual("9", amazonNode.Id);
        }

        [Test]
        public void SetsAmazonNodeLabelToAmazon()
        {
            Assert.AreEqual("Amazon", amazonNode.Label);
        }

        [Test]
        public void SetsAmazonNodeAdjacentNodesTo10()
        {
            CollectionAssert.AreEquivalent(new[] { "10" }, amazonNode.AdjacentNodes);
        }

        [Test]
        public void SetsAppleNodeIdTo1()
        {
            Assert.AreEqual("1", appleNode.Id);
        }

        [Test]
        public void SetsAppleNodeLabelToApple()
        {
            Assert.AreEqual("Apple", appleNode.Label);
        }

        [Test]
        public void SetsAppleNodeAdjacentNodesTo3And2()
        {
            CollectionAssert.AreEquivalent(new[] { "2", "3" }, appleNode.AdjacentNodes);
        }

        [Test]
        public void SetsDbAdjacentNodesString()
        {
            Assert.AreEqual("2;3", appleNode.DbAdjacentNodesString);
        }
    }
}
