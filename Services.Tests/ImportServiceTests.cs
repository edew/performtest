﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using FakeItEasy;
using NUnit.Framework;

namespace Services.Tests
{
    [TestFixture]
    public class ImportServiceTests
    {
        [Test]
        public void CallsSetNodesWithNodesFromInputXml()
        {
            var nodesService = A.Fake<INodesService>();

            var xmlImport = new ImportService(nodesService);

            var xml = new[] { File.ReadAllText("TestData\\amazon.xml") };
            var nodes = xml.Select(x => new Node(x));

            xmlImport.ImportNodes(xml);

            A.CallTo(() => nodesService.SetNodes(A<Node[]>.That.Matches(n => n.Single().Id == nodes.Single().Id)))
                .MustHaveHappened();
        }
    }
}
