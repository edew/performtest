﻿using FakeItEasy;
using NUnit.Framework;

namespace Services.Tests
{
    [TestFixture]
    public class JsonServiceTests
    {
        private string json;

        [SetUp]
        public void SetUp()
        {
            var node = new Node {Id = "1337", Label = "Hi!", AdjacentNodes = new[] {"5", "7"}};

            var nodesService = A.Fake<INodesService>();
            A.CallTo(() => nodesService.GetNodes()).Returns(new[] {node});

            var jsonService = new JsonService(nodesService);

            json = jsonService.GetNodesJson();
        }

        [Test]
        public void GetJsonGetsNodesFromNodesService()
        {
            var nodesService = A.Fake<INodesService>();

            var jsonService = new JsonService(nodesService);

            jsonService.GetNodesJson();

            A.CallTo(() => nodesService.GetNodes()).MustHaveHappened();
        }

        [Test]
        public void DoesNotIncludeDbIdPropertyInJson()
        {
            StringAssert.DoesNotContain("DbId", json);
        }

        [Test]
        public void DoesNotIncludeDbAdjacentNodesStringInJson()
        {
            StringAssert.DoesNotContain("DbAdjacentNodesString", json);
        }

        [Test]
        public void GetJsonIncludesIdLabelAndAdjacentNodes()
        {
            const string expected = "[{\"Id\":\"1337\",\"Label\":\"Hi!\",\"AdjacentNodes\":[\"5\",\"7\"]}]";

            Assert.AreEqual(expected, json);
        }
    }
}
